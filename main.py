import pandas as pd
import requests
import json

URL = "https://raw.githubusercontent.com/MinCiencia/Datos-COVID19/master/output/producto5/TotalesNacionales.csv"


def download_file(url, header):
    local_filename = url.split('/')[-1]
    # NOTE the stream=True parameter below
    with requests.get(url, stream=True, headers=header) as r:
        r.raise_for_status()
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192): 
                # If you have chunk encoded response uncomment if
                # and set chunk_size parameter to None.
                #if chunk: 
                f.write(chunk)
    return local_filename

if __name__ == "__main__":

    # Extract part of the ETL process
    headers = {
        "Accept" : "application/json",
        "Content-Type" : "text/csv"
    }

    file = download_file(URL, headers)

    print("Se ha descargado el archivo: {}".format(file))

    df = pd.read_csv (file)
    print(df)


